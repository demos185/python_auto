## Demo Project:
Data Backup & Restore
## Technologies used:
Python, Boto3, AWS
## Project Decription:
* Write a Python script that automates creating backups for EC2 Volumes
* Write a Python script that cleans up old EC2 Volume snapshots
* Write a Python script that restores EC2 Volumes
## Description in details:
__Step 1:__ Create EC2 instances (first with tag prod, second with tag dev)
### Write a Python script that automates creating backups for EC2 Volumes
Create new file and import `boto3`

__Step 1:__ Describe volumes (it gives list of information about volumes)
1. create variable and print list of volumes
```py
volumes = ec2_describe_volumes()
print(volumes['Volumes'])
```
2. Create `for` loop that creates snapshot
```py
for volume in volumes['Volumes']:
    new_snapshot = ec2_client.create_snapshot(
        VolumeId=volume['VolumeId']
    )
```

3. Write scheduler 
   * import `schedule` library
   * create function (put code that creates volume inside it)
   * add schecule
```py
import boto3
import schedule

ec2_client = boto3.client('ec2', region_name="eu-west-3")

def create_volume_snapshots():
    volumes = ec2_describe_volumes()
    for volume in volumes['Volumes']:
        new_snapshot = ec2_client.create_snapshot(
            VolumeId=volume['VolumeId']
        )

schedule.every().day.do(create_volume_snapshots)

while True:
    schedule.run_pending()
```

4. Back up only Prod servers
   * Add tag `prod` to your prod server (in this example you cave 1 machine so you can add ot manualy using AWS UI)
   * add filter
```py
def create_volume_snapshots():
    volumes = ec2_describe_volumes(
        Filters=[
            {
                'Name': 'tag:Namde',
                'Values': ['prod']
            }
        ]
    )
```
#### Completed code:
```py
import boto3
import schedule

ec2_client = boto3.client('ec2', region_name="eu-west-3")

def create_volume_snapshots():
    volumes = ec2_describe_volumes(
        Filters=[
            {
                'Name': 'tag:Namde',
                'Values': ['prod']
            }
        ]
    )
    for volume in volumes['Volumes']:
        new_snapshot = ec2_client.create_snapshot(
            VolumeId=volume['VolumeId']
        )

schedule.every().day.do(create_volume_snapshots)

while True:
    schedule.run_pending()
```
---
### Write a Python script that cleans up old EC2 Volume snapshots
__Step 1:__ Create new file for script
1. You want to clean snapshots created by you not amazon so you need use `OwnerIds` parameter and assig `self`
```py
import boto3

ec2_client = boto3.client('ec2', region_name="eu-west-3")

snapshots = ec2_client.describe_snapshots(
    OwnerIds=['self']   
)
```
2. Sort snapshots by the date (just for visual)
   * write sort command and use `itemgetter` (import this function from `operator`)
```py 
sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)

for snap in sorted_by_date:
    print(snap["StartTime"])
```
3. Delete 2 old snapshots
```py
for snap in sorted_by_date[2:]:
    response = ec2_client.delete_snapshot(
        SnapshotId=snap['SnapshotId']
    )
    print(response)
```
4. Cleanup for multiple Volumes
   * grab function from `backups` script and write for loop (where you paste all logic for clean up)
```py
volumes = ec2_describe_volumes(
   Filters=[
       {
           'Name': 'tag:Namde',
           'Values': ['prod']
       }
   ]
)
for volume in ['Volumes']:
    snapshots = ec2_client.describe_snapshots(
        OwnerIds=['self'],
           Filters=[
               {
                   'Name': 'volume-id',
                   'Values': [volume['VolumeId']]
               }
   ]
    )
    sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)


    for snap in sorted_by_date[2:]:
        response = ec2_client.delete_snapshot(
            SnapshotId=snap['SnapshotId']
        )
        print(response)
```

#### Completed code:
```py
import boto3
from operator import itemgetter
 
ec2_client = boto3.client('ec2', region_name="eu-west-3")

# Cleanup for multiple volumes
volumes = ec2_describe_volumes(
   Filters=[
       {
           'Name': 'tag:Namde',
           'Values': ['prod']
       }
   ]
)
for volume in ['Volumes']:
    snapshots = ec2_client.describe_snapshots(
        OwnerIds=['self'],
           Filters=[
               {
                   'Name': 'volume-id',
                   'Values': [volume['VolumeId']]
               }
   ]
    )
    sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)


    for snap in sorted_by_date[2:]:
        response = ec2_client.delete_snapshot(
            SnapshotId=snap['SnapshotId']
        )
        print(response)


""" cleanup for single snapshot
snapshots = ec2_client.describe_snapshots(
    OwnerIds=['self']   
)

sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)


for snap in sorted_by_date[2:]:
    response = ec2_client.delete_snapshot(
        SnapshotId=snap['SnapshotId']
    )
    print(response)
"""
```
### Write a Python script that restores EC2 Volumes
_Create new file with python script_

__Step 1:__ Attach new volume created by you from snapshots to specific instance 
1. Add  `ec2_resource` and create instance ID
```py
import boto3

ec2_client = boto3.client('ec2', region_name="eu-west-3")
ec2_resource = boto3.client('ec2', region_name="eu-west-3")

instance_id = "INSTANCE_ID_HERE"
```
2. Create filter to filter the volumes that belong to specyfic instance
```py
volumes = ec2_client.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [instance_id]
        }
    ]
)

instance_volume = volumes['volumes'][0]
```
3. Grab the latest snapshot of that volume (use `itemgetter` for sort)
```py
snapshots =ec2_client.decribe_snapshots(
    OwnerIds=['self'],
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [instance_volume['VolumeId']]
        }
    ]
)

latest_snapshot = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)
```
4. Create new volume from that snapshot
```py
ec2_client.create_volume(
    SnapshotId=latest_snapshot['Snapshot'],
    AvailabilityZone="eu-west-3"
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'prod'
                }
            ]
        }
    ]
)
```
5. Attach new volume to the EC2 instance
```py
ec2_resource.Instance(instance_id).attach_volime(
    VolumeId=new_volume['VolumeId'],
    Device='/dev/xvdb'
```
>-  `Device` : chage name here otherwise you'll get error (existing volume name)
6. Avoid time problems( volume need time to be ready for attachement)
```py
while True:
    vol = ec2_resource.Volume(new_volume['VolumeId'])
    print(vol.state)
    if vol.state == 'available':
        ec2_resource.Instance(instance_id).attach_volume(
            VolumeId=new_volume['VolumeId'],
            Device='/dev/xvdb'
        )
        break
```
#### Completed code:

```py
import boto3
from operatot import itemgetter

ec2_client = boto3.client('ec2', region_name="eu-west-3")
ec2_resource = boto3.client('ec2', region_name="eu-west-3")

instance_id = "INSTANCE_ID_HERE"

volumes = ec2_client.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [instance_id]
        }
    ]
)

instance_volume = volumes['volumes'][0] 

snapshots =ec2_client.decribe_snapshots(
    OwnerIds=['self'],
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [instance_volume['VolumeId']]
        }
    ]
)

latest_snapshot = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)

new_volume = ec2_client.create_volume(
    SnapshotId=latest_snapshot['Snapshot'],
    AvailabilityZone="eu-west-3"
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'prod'
                }
            ]
        }
    ]
)

ec2_resource.Instance(instance_id).attach_volime(
    VolumeId=new_volume['VolumeId'],
    Device='/dev/xvdb'
)
```
