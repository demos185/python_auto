## Demo Project:
Website Monitoring and Recovery
## Technologies used:
Python, Linode, Docker, Linux
## Project Decription:
* Create a server on a cloud platform
* Install Docker and run a Docker container on the remote server
* Write a Python script that monitors the website by accessing it and validating the HTTP response
* Write a Python script that sends an email notification when website is down
* Write a Python script that automatically restarts the application & server when the application is down
## Description in details:
### Create a server on a cloud platform (in Example Linode)
__Step 1:__ Add a Linode
1. Image: Debian
2. Region: Frankfurt 
3. Plan: 2GB
4. Root password:
5. Add SSH key: 
6. Create
### Install Docker and run a Docker container on the remote server
__Step 1:__ Connect to the server via ssh and install docker and run docker container
1. Install docker on debian ([official documentation](https://docs.docker.com/engine/install/debian/))
2. Run nginx on server
```sh
docker run -d -p 8080:80 nginx
```
### Write a Python script that monitors the website by accessing it and validating the HTTP response
__Step 1:__ Create new file for script
1. Install `requests` library (if you don't have it on your local machine)
```sh
pip install requests
```
2. Import library into the script
3. Make a request to your nginx application
```py
response = requests.get('URL_OF_NGINX')
```
4. Write status check
```py
import requests

response = requests.get('URL_OF_NGINX')
if response.status_code == 200:
    print('Application is running succesfully!')
else:
    print('Application Down. Fix it!')
```
### Write a Python script that sends an email notification when website is down
__Step 1:__ Send email

1. Write logic for sending email

__Note:__ import `smtplib` library!
```py
else:
    print('Application Down. Fix it!')
    # send email to me
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        msg = "Subject: SITE DOWN\nFix the issue"
        stmp.sendemail(EMAIL_ADRESS, EMAIL_ADDRESS, msg)        
```
>- `smtp.starttls()` - will start basically encrypt the comunication from python to your emal server
>- `smtp.ehlo()` - basically identifies or python application with the email server on this encrypted communication or connection 
>- `smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)` - Gmail have two-way verification
>   1. Without two-way verification:
>      * Provide email and password in python code
>      * Enable "less secure apps" in your Google account
>
>      __Note :__ You need to allow other programs like python to access your account. You can do that by switching this thing on, allow less secure app (It's not the best security practice, especially if this your private account or some important account where you keep some sensitive data but that's the only way you can allow applications to send emails)
>
>   2. With two-way verification:
>       * Create application specific password
>
>- `stmp.sendemail(EMAIL_ADRESS, EMAIL_ADDRESS)` - here you assign "from" and "to" sending
2. Create environment variables ans import them (import module `os`)
```py
import os   

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
```
__Note:__ you can write that variables inside your `~/.bash_profile` or use PyCharm

__Step 2:__ Handle Connection Errors
1. Add `try` and `except` 
    `try`  It try to execute code whatever inside the try block. if something happends then executes `except` block
```py
try:
  response = requests.get('URL_OF_NGINX')
  if response.status_code == 200:
      print('Application is running successfully!')
  else:
      print('Application Down. Fix it!')
      msg = f'Application returned {response.status_code}'
      send_notification(msg)
      restart_container()
except Exception as ex:
  print(f'Connection error happened: {ex}')
  msg = 'Application not accessible at all'
  send_notification(msg)
  restart_server_and_container()
```
2. Create function for sending email and paste logic into it 
```py
def send_notification(email_msg ):
    print('Sending an email...')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SITE DOWN\n{email_message}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)
```
### Write a Python script that automatically restarts the application & server when the application is down
__Step 1:__ Connect to the Linode server using ssh  from python program
1. Write logic 
```py 
try:
  response = requests.get('URL_OF_NGINX')
  if response.status_code == 200:
      print('Application is running successfully!')
  else:
      print('Application Down. Fix it!')
      msg = f'Application returned {response.status_code}'
      send_notification(msg)
      restart_container()

      # restart the application
      ssh = paramiko.SSHClient()
      ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
      ssh.connect(hostname='HOST_URL', username='root', key_filename='absolute_path_to_private_key')
      stdin, stdout, strerr = ssh.exec_command('docker start CONTAINER_ID')
      print (output.readlines())
      ssh.close
```
__Paramiko__
- Python implementation of SSHv2
- library for making SSH connections (client or server)

__Important:__ install `paramiko` and import it!

__Step 2:__ Write restart server logic (In this exaple used Linode and logic is specific for Linode)
1. Install `Linode_api4` library and import it 
2. Write logic
```py  
except Exception as ex:
  print(f'Connection error happened: {ex}')
  msg = 'Application not accessible at all'
  send_notification(msg)

  # restart linode server 
  print('Rebooting the server')
  client = linode_api4.LinodeClient(LINODE_TOKEN)
  nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
  nginx_server.reboot()
```
3. Create API token (you cand create it in Linode UI) with read/write permissions and set it as environmental variable

__Step 3:__ Restart Docker container
1. create function for restarting container
```py
def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='NGINX_URL', username='root', key_filename='PATH_TO_YOU_KEY')
    stdin, stdout, stderr = ssh.exec_command('docker start CONTAINER_ID')
    print(stdout.readlines())
    ssh.close()
```
2. Create while loop for avoiding rebooting issues
```py
while True:
    nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
    if ngins_server.status == 'running':
        restart_container()
        break 
```
3. To be sure you can add `time` module (import it)
```py
while True:
    nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
    if ngins_server.status == 'running':
        time.sleep(5)
        restart_container()
        break 
```
__Step 4:__ Cleanup code
1. you can group `restart` using function
```py
def restart_server_and_container():
    # restart linode server
    print('Rebooting the server...')
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
    nginx_server.reboot()

    # restart the application
    while True:
        nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
        if nginx_server.status == 'running':
            time.sleep(5)
            restart_container()
            break

```

__Step 5:__ Schedule monitoring task
1. import schedule
2. Create function
```py
def monitor_application():
    try:
        response = requests.get('NGINX_URL')
        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application Down. Fix it!')
            msg = f'Application returned {response.status_code}'
            send_notification(msg)
            restart_container()
    except Exception as ex:
        print(f'Connection error happened: {ex}')
        msg = 'Application not accessible at all'
        send_notification(msg)
        restart_server_and_container()

schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()
```

### Completed Code:
```py
import requests
import smtplib
import os
import paramiko
import linode_api4
import time
import schedule

EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
LINODE_TOKEN = os.environ.get('LINODE_TOKEN')


def restart_server_and_container():
    # restart linode server
    print('Rebooting the server...')
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
    nginx_server.reboot()

    # restart the application
    while True:
        nginx_server = client.load(linode_api4.Instance, LINODE_ID_HERE)
        if nginx_server.status == 'running':
            time.sleep(5)
            restart_container()
            break


def send_notification(email_msg):
    print('Sending an email...')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SITE DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)


def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='URL_OF_YOUR_SERVER', username='root', key_filename='ABSOLUTE_PATH_TO_KEY')
    stdin, stdout, stderr = ssh.exec_command('docker start CONTAINER_ID')
    print(stdout.readlines())
    ssh.close()


def monitor_application():
    try:
        response = requests.get('NEXUS_URL_HERE')
        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application Down. Fix it!')
            msg = f'Application returned {response.status_code}'
            send_notification(msg)
            restart_container()
    except Exception as ex:
        print(f'Connection error happened: {ex}')
        msg = 'Application not accessible at all'
        send_notification(msg)
        restart_server_and_container()


schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()
```