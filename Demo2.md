## Demo Project:
Automate configuring EC2 Server Instances
## Technologies used:
Python, Boto3, AWS
## Project Decription:
* Write a Python script that automates adding environment tags to all EC2 Server instances
## Description in details:

__Note:__ _In this example used 2 machines in Paris for prod and 1 machine in Frankfurt_

__Step 1:__ Create new file for script
1. Import `boto3` and write region
```py
import boto3

ec2_client_paris = boto3.client(`ec2`, region_name="eu-west-3")
```
2. Add `describe_instance_status` (use official documentation all the time)
3. Create list for IDs
```py
instance_ids_paris = []
```
4. Collect all instances ids into a list 
```py
for ins in instances:
  instance_ids_paris.spend(ins['InstanceId']) 
```
5. Add resource
```py
ec2_resource_paris = boto3.resource('ec2, region_name="eu-west-3"')
```
6. Add tags for all instances __at once__ (using documintation add banch of code and eddit it)
```py
response = ec2_resource_paris.create_tags(
    Resources=instance_ids_paris,
    Tags=[
        {
            'Key': 'string',
            'Value': 'string'
        },
    ]
)
```
7. Repeat all steps for Frankfurt

## Completed code:
```py
import boto3

ec2_client_paris = boto3.client(`ec2`, region_name="eu-west-3")
ec2_resource_paris = boto3.resource('ec2, region_name="eu-west-3"')

ec2_client_frankfurt = boto3.client(`ec2`, region_name="eu-west-1")
ec2_resource_frankfurt = boto3.resource('ec2, region_name="eu-west-1"')

instance_ids_paris[]
instance_ids_frankfurt[]

reservation_paris =  ec2_client_paris.describe_instance_status()['Reservations']
for res in reservation_paris
    instances = res['Instances']
    for ins in instances:
        instance_ids_paris.spend(ins['InstanceId'])

response = client.create_tags(
    Resources=instance_ids_paris,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
) 

reservation_frankfurt =  ec2_client_frankfurt.describe_instance_status()['Reservations']
for res in reservation_frankfurt
    instances = res['Instances']
    for ins in instances:
        instance_ids_frankfurt.spend(ins['InstanceId'])

response = client.create_tags(
    Resources=instance_ids_frankfurt,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
) 
```