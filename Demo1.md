## Demo Project:
Health Check:EC2 Status Checks
## Technologies used:
Python, Boto3, AWS, Terraform
## Project Decription:
* Create EC2 Instances with Terraform
* Write a Python script that fetches statuses of EC2 Instances and prints to the console
* Extend the Python script to continuously check the status of EC2 Instances in a specific interval
## Description in details:
### Create EC2 Instances with Terraform (use terraform project from terraform demo)

__Step 1:__ Prepare terraform project (in thix exaple used 3 instances)
1. Create another 2 instances in `main.tf` (just copy already created instance and change name)
2. Configure variables
3. Set region
```go
provider "aws" {
    region = "eu-central-1"
}
```
4. terraform plan/apply

__Step 2:__ import boto3 and configure it
```py
import boto3

ec2_client = boto3.client(`ec2`, region_name="eu-central-1")
ec2_resource = boto3.resource('ec2' region_name="eu-central-1")
```
__Step 3:__ Configure script 

```py
reservations = ec2_instance.describe_instances()
for reservation in reservations['Reservations']:
    instances = reservation['Instances']
    for instance in instances:
        print(f"Status of instance {instance['InstanceId']} is {instance['State']['Name']}")
```
- `Reservations` in boto3: 

### Write a Python script that fetches statuses of EC2 Instances and prints to the console
__Step 1:__ Call another function for Status check
```py
statuses = ec2_client.describe_instance_status()
for status in statuses['InstanceStatuses']:
    ins_status = status['InstanceStatuses']['Status']
    sys_status = status['SystemStatus']['Status']
    print(f"Instance {status['InstanceId']} status is {ins_status} and system status is {sys_tatus}")
```
__Step 2:__ Get everythin in 1 AWS API call(optional)
```py
import boto3

ec2_client = boto3.client(`ec2`, region_name="eu-central-1")
ec2_resource = boto3.resource('ec2' region_name="eu-central-1")


statuses = ec2_client.describe_instance_status()
for status in statuses['InstanceStatuses']:
    ins_status = status['InstanceStatuses']['Status']
    sys_status = status['SystemStatus']['Status']
    state = status['InstaceState']['Name']
    print(f"Instance {status['InstanceId']} is {state} with instace status is {ins_status} and system status is {sys_tatus}")
```
### Extend the Python script to continuously check the status of EC2 Instances in a specific interval
__Step 1:__ Write scheduler that will execute logic every 5 minutes
1. Install and import   `Schedule`
2. create function
```py
def check_instance_status():
    statuses = ec2_client.describe_instance_status()
    for status in statuses['InstanceStatuses']:
        ins_status = status['InstanceStatuses']['Status']
        sys_status = status['SystemStatus']['Status']
        state = status['InstaceState']['Name']
        print(f"Instance {status['InstanceId']} is {state} with instace status is {ins_status} and system status is {sys_tatus}")
```
3. Add scheduler(for demonstration you can change minutes on seconds)
```py
schedule.every(5).minutes.do(check_instance_status)
```
4. run sceduler
```py
while true:
    schedule.run_pending()
```
>__Important:__  `statuses = ec2_client.describe_instance_status()` shows only `running` states by default, but you can configure it for showing all statuses (see docs)
>```py
>def check_instance_status():
>    statuses = ec2_client.describe_instance_status(
>        IncludeAllInstances=True
>    )
>    for status in statuses['InstanceStatuses']:
>```

### Completed code:
```py
import boto3

ec2_client = boto3.client(`ec2`, region_name="eu-central-1")
ec2_resource = boto3.resource('ec2' region_name="eu-central-1")


def check_instance_status():
    statuses = ec2_client.describe_instance_status()
    for status in statuses['InstanceStatuses']:
        ins_status = status['InstanceStatuses']['Status']
        sys_status = status['SystemStatus']['Status']
        state = status['InstaceState']['Name']
        print(f"Instance {status['InstanceId']} is {state} with instace status is {ins_status} and system status is {sys_tatus}")

schedule.every(5).minutes.do(check_instance_status)

while true:
    schedule.run_pending()
```
