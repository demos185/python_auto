## Demo Project:
Automate displaying EKS cluster information
## Technologies used:
Python, Boto3 ,AWS EKS
## Project Decription:
* Write a Python script that fetches and displays EKS cluster status and information
## Description in details:
__Step 1:__ Create EKS cluster with TF (Use TF file from TF demo)

__Step 2:__ Create python script
1. import boto3
2. Switch to EKS client
```py
client = boto3.client('eks', region_name="eu-west-3")
``` 
3. Define cluster
```py
clusters = client.list_clusters()['clusters']
```
4. Get status
```py
for cluster in clusters
    response = client.describe_cluster(
        name=cluster
    )
    cluster_info = responce['cluster']
    claster_status = cluster_info['status']
    print(f"Cluster {cluster} status is {cluster_status}")
```
5. Get endpoint and version(repeat step `4` for endpoint and version)
### Completed code:
```py
import boto3

client = boto3.client('eks', region_name="eu-west-3")
clusters = client.list_clusters()

for cluster in clusters
    response = client.describe_cluster(
        name=cluster
    )
    cluster_info = responce['cluster']
    claster_status = cluster_info['status']
    cluster_endpoint = cluster_info['endpoint']
    cluster_version = cluster_info['version']
    print(f"Cluster {cluster} status is {cluster_status}")
    print(f"Cluster {cluster} endpoint: {cluster_endpoint}")
    print(f"Cluster {cluster} version: {cluster_version}")
```
