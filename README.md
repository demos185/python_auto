## List of Demos

__1.__ Health Check:EC2 Status Checks

__2.__ Automate configuring EC2 Server Instances

__3.__ Automate displaying EKS cluster information

__4.__ Data Backup & Restore

__5.__ Website Monitoring and Recovery